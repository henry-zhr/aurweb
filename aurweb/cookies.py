from fastapi import Request
from fastapi.responses import Response

from aurweb import config


def samesite() -> str:
    """Produce cookie SameSite value.

    Currently this is hard-coded to return "lax"

    :returns: "lax"
    """
    return "lax"


def timeout(extended: bool) -> int:
    """Produce a session timeout based on `remember_me`.

    This method returns one of AUR_CONFIG's options.persistent_cookie_timeout
    and options.login_timeout based on the `extended` argument.

    The `extended` argument is typically the value of the AURREMEMBER
    cookie, defaulted to False.

    If `extended` is False, options.login_timeout is returned. Otherwise,
    if `extended` is True, options.persistent_cookie_timeout is returned.

    :param extended: Flag which generates an extended timeout when True
    :returns: Cookie timeout based on configuration options
    """
    timeout = config.getint("options", "login_timeout")
    if bool(extended):
        timeout = config.getint("options", "persistent_cookie_timeout")
    return timeout


def update_response_cookies(
    request: Request,
    response: Response,
    aurtz: str = None,
    aurlang: str = None,
    aursid: str = None,
    aurremember: bool = None,
) -> Response:
    """Update session cookies. Modify the value if needed, otherwise just
    refresh the expiration.

    The values are retrieved from `request` if not specified in parameters.
    This may overwrite previously set values, so this method should be called at
    most once for each `response`.

    The AURSID cookie's expiration is based on `aurremember`, while other
    cookies are almost permanent.

    :param request: FastAPI request
    :param response: FastAPI response
    :param aurtz: Optional AURTZ cookie value
    :param aurlang: Optional AURLANG cookie value
    :param aursid: Optional AURSID cookie value
    :param aurremember: Optional AURREMEMBER cookie value
    :returns: Updated response
    """
    secure = config.getboolean("options", "disable_http_login")
    permanent_cookie_timeout = config.getint("options", "permanent_cookie_timeout")
    if aurtz is None:
        aurtz = request.cookies.get("AURTZ", None)
    if aurlang is None:
        aurlang = request.cookies.get("AURLANG", None)
    if aurremember is None and "AURREMEMBER" in request.cookies:
        aurremember = request.cookies.get("AURREMEMBER") == "1"
    if aursid is None and request.user.is_authenticated():
        aursid = request.cookies.get("AURSID")
    if aurtz is not None:
        response.set_cookie(
            "AURTZ",
            aurtz,
            secure=secure,
            httponly=secure,
            max_age=permanent_cookie_timeout,
            samesite=samesite(),
        )
    if aurlang is not None:
        response.set_cookie(
            "AURLANG",
            aurlang,
            secure=secure,
            httponly=secure,
            max_age=permanent_cookie_timeout,
            samesite=samesite(),
        )
    if aursid is not None:
        response.set_cookie(
            "AURSID",
            aursid,
            secure=secure,
            httponly=secure,
            max_age=timeout(aurremember),
            samesite=samesite(),
        )
    if aurremember is not None:
        response.set_cookie(
            "AURREMEMBER",
            "1" if aurremember else "0",
            secure=secure,
            httponly=secure,
            max_age=permanent_cookie_timeout,
            samesite=samesite(),
        )
    return response
